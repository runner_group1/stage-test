#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "funzioni.h"

TEST_CASE("sequenze di due operazioni") {
    int init = GENERATE(take(1000,random(-10000,10000)));
    int a = init;
    SECTION("incremento") {
        inc(&a);
        REQUIRE(a == (init + 1));
        SECTION("incremento"){
           inc(&a);
           REQUIRE(a == (init + 2)); 
        }
        SECTION("decremento"){
           dec(&a);
           REQUIRE(a == init); 
        }
    }
    SECTION("decremento") {
        dec(&a);
        REQUIRE(a == (init - 1));
        SECTION("incremento"){
           inc(&a);
           REQUIRE(a == init); 
        }
        SECTION("decremento"){
           dec(&a);
           REQUIRE(a == (init - 2)); 
        }
    }
}

