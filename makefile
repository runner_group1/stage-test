eseguibile_test: test.o funzioni.o
	g++ -o eseguibile_test test.o funzioni.o

test.o: test.cpp funzioni.h
	g++ -c test.cpp

funzioni.o: funzioni.cpp funzioni.h
	g++ -c funzioni.cpp
